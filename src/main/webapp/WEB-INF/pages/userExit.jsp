<%@ page contentType="text/html; charset=ISO-8859-1" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<html>
    <head>
        <title>Spring MVC Form Handling</title>
    </head>
    <body>
        <h2>User Registration Result</h2>
        <mvc:form modelAttribute="user" action="enter.mvc">
            <table>
                <tr>
                    <td>Teams</td>
                    <td>${userModel.teams}</td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td>${userModel.gender}</td>
                </tr>

                <tr>
                    <td>Team coconut</td>
                    <td>${teamCoconut}</td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="Back" />
                    </td>
                </tr>
            </table>
        </mvc:form>
    </body>
</html>