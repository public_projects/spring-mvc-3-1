<%@ page contentType="text/html; charset=ISO-8859-1" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<html>
    <head>
        <title>Spring MVC Form Handling</title>
    </head>
    <body>
        <h2>User Registration Form</h2>
        <mvc:form modelAttribute="user" action="exit.mvc">
            <table>
                <tr>
                    <td><mvc:label path="teams">TeamCoconut</mvc:label></td>
                    <td><mvc:checkboxes path="teams" items="${teamCoconut}" /></td>
                </tr>

                <tr>
                    <td><mvc:label path="teams">Team fm user</mvc:label></td>
                    <td><mvc:checkboxes path="teams" items="${user.teams}" /></td>
                </tr>

                <tr>
                    <td><mvc:label path="age">Age</mvc:label></td>
                    <td><mvc:input path="age" value="${user.age}"/></td>
                    <td><mvc:input path="age"/></td>
                </tr>

                <tr>
                    <td><mvc:label path="gender">Team</mvc:label></td>
                    <td><mvc:radiobuttons path="gender" items="${genders}" /></td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="Submit" />
                    </td>
                </tr>
            </table>
        </mvc:form>
    </body>
</html>
