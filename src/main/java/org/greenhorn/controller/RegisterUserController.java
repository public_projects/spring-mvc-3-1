package org.greenhorn.controller;

import java.util.ArrayList;
import java.util.List;
import org.greenhorn.entity.Gender;
import org.greenhorn.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author greenhorn
 */
@Controller
public class RegisterUserController {

    /**
     * method level
     *
     * @return
     */
    @ModelAttribute("teamCoconut")
    public List<String> teamCoco() {
        List<String> teams = new ArrayList<>();
        teams.add("aa");
        teams.add("bb");
        teams.add("cc");
        teams.add("dd");
        return teams;
    }

    @ModelAttribute("genders")
    public Gender[] gender() {
        return Gender.values();
    }

    @RequestMapping(value = "/enter")
    public ModelAndView initiate() {

        User user = new User();
        List<String> fruits = new ArrayList<>();
        fruits.add("apple");
        fruits.add("orange");
        fruits.add("banana");
        user.setTeams(fruits);
        user.setAge(10);

        ModelAndView modelAndView
                = new ModelAndView("userEnter", "user", user);

        return modelAndView;
    }

    /**
     * method's argument level
     *
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/exit")
    public ModelAndView end(
            @ModelAttribute(value = "teams") List<String> teams,
            @ModelAttribute(value = "gender") Gender gender,
            @ModelAttribute(value = "age") int age,
            User user) {

        System.err.println("start");
        System.err.println("Gender: " + gender);
        System.err.println("age: " + age);
        for (String value : user.getTeams()) {
            System.err.println("team frm user: " + value);
        }

        for (String team : teams) {
            System.err.println("team directly: " + team);
        }
        System.err.println("end");
        System.err.println("");

        ModelAndView modelAndView
                = new ModelAndView("userExit");
        modelAndView.addObject("userModel", user);

        return modelAndView;
    }

}
